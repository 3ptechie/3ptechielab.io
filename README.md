# Portfolio

Always maintain an up to date portfolio of your skills, qualifications, experiences and work.

## Components
- Business Card
- Cover Letter
- Credentials
- Resume
- Website

## Links
* GitLab Repo - https://gitlab.com/3ptech/portfolio
